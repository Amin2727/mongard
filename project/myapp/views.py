from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from .models import Todo
from django.contrib import messages
from .forms import TodoCreateForm, TodoUpdateForm

def home(request):
    context={
        "todos": Todo.objects.filter(status=True)
    }
    return render(request, "myapp/home.html",context)

# def home2(request):
#     # person = {'name':'Amin', 'lastname':'Hezarkhani'}
#     return render(request, "myapp/home2.html", {'name':'amin', 'lastname':'hezarkhani', 'value':2, 'p':'Django!'})

def detail(request, todo_id):
    context = {
        "todo": get_object_or_404(Todo, id=todo_id, status=True)

    }
    return render(request, "myapp/detail.html", context)


def delete(request, todo_id):
    Todo.objects.get(id=todo_id).delete()
    messages.success(request, 'Your todo was successfully deleted!', 'success')
    return redirect("myapp:home")


def create(request):
    if request.method == 'POST':
        form = TodoCreateForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            Todo.objects.create(title=cd['title'], body=cd['body'], created=cd['created'], status=cd['status'])
            messages.success(request, 'Your Todo created successfully!', 'success')
            return redirect('myapp:home')
    else:
       form = TodoCreateForm()
    return render (request, 'myapp/create.html', {'form':form})


#این جا خیلی مهم و یه مقدار پیچیده هستش و برای جلسه ۱۹ هستش
def update(request, todo_id):
    todo = Todo.objects.get(id=todo_id)
    if request.method == 'POST':
        form = TodoUpdateForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            messages.success(request, 'Your Todo updated successfully!', 'success')
            return redirect('myapp:detail', todo_id)
    else:
        form = TodoUpdateForm(instance=todo)
    return render(request, 'myapp/update.html', {'form':form})