from django.urls import path
from .views import (
    home, 
    detail, 
    delete, 
    create,
    update,
)

app_name='myapp'
urlpatterns = [
    path('', home, name="home"),
    path('detail/<int:todo_id>/', detail, name="detail"),
    path('delete/<int:todo_id>/', delete, name="delete"),
    path('update/<int:todo_id>/', update, name="update"),
    path('create/', create, name="create"),
]
